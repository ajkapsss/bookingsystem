// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase:{
  	apiKey: "AIzaSyC_Tz1f4LgvAXfbB_Gr5dvS6IMQi0BLdIM",
    authDomain: "fir-clouddemo-a9620.firebaseapp.com",
    databaseURL: "https://fir-clouddemo-a9620.firebaseio.com",
    projectId: "fir-clouddemo-a9620",
    storageBucket: "fir-clouddemo-a9620.appspot.com",
    messagingSenderId: "277303023893"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

export class Booking{
	constructor(
	public startDate: Date,
	public endDate: Date,
	public name: string,
	public environment: string,
	){}
}
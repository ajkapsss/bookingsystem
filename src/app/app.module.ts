import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { BookingDetailsComponent } from './booking-details/booking-details.component';
import { BookingSearchComponent } from './booking-search/booking-search.component';
import { BookingPageComponent } from './booking-page/booking-page.component';
import { FormControl, 
         Validators, 
         ValidatorFn, 
         AbstractControl } from '@angular/forms';
import { FormsModule, 
         ReactiveFormsModule, 
         NgForm } from '@angular/forms';
import { MatDatepickerModule,
         MatNativeDateModule,
         MatInputModule} from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment'



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BookingDetailsComponent,
    BookingSearchComponent,
    BookingPageComponent
      ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatTabsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    AppRoutingModule,
    MatExpansionModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatInputModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule
],
  providers: [MatNativeDateModule,],
  bootstrap: [AppComponent]
})
export class AppModule { }
